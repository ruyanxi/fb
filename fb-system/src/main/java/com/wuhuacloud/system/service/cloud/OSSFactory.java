package com.wuhuacloud.system.service.cloud;

import com.wuhuacloud.common.constant.ConfigConstant;
import com.wuhuacloud.common.constant.Constants;
import com.wuhuacloud.common.utils.spring.SpringUtils;
import com.wuhuacloud.system.service.ISysConfigService;

/**
 * 文件上传Factory
 *
 * @author
 */
public final class OSSFactory {
    private static ISysConfigService sysConfigService;

    static {
        OSSFactory.sysConfigService = (ISysConfigService) SpringUtils.getBean("sysConfigServiceImpl");
    }

    public static CloudStorageService build(){
        //获取云存储配置信息
        CloudStorageConfig config = sysConfigService.getConfigObject(ConfigConstant.CLOUD_STORAGE_CONFIG_KEY, CloudStorageConfig.class);

        if(config.getType() == Constants.CloudService.QINIU.getValue()){
            return new QiniuCloudStorageService(config);
        }else if(config.getType() == Constants.CloudService.ALIYUN.getValue()){
            return new AliyunCloudStorageService(config);
        }else if(config.getType() == Constants.CloudService.QCLOUD.getValue()){
            return new QcloudCloudStorageService(config);
        }

        return null;
    }

}
