package com.wuhuacloud.system.service.cloud;

import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;
import com.wuhuacloud.common.exception.UtilException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.math3.geometry.partitioning.Region;

import java.io.IOException;
import java.io.InputStream;

/**
 * 七牛云存储
 *
 * @author
 */
public class QiniuCloudStorageService extends CloudStorageService {
    private UploadManager uploadManager;
    private String token;


    private BucketManager bucketManager;

    public QiniuCloudStorageService(CloudStorageConfig config){
        this.config = config;

        //初始化
        init();
    }

    private void init(){
        uploadManager = new UploadManager(new Configuration(Zone.autoZone()));
        token = Auth.create(config.getQiniuAccessKey(), config.getQiniuSecretKey()).
                uploadToken(config.getQiniuBucketName());

        bucketManager = new BucketManager(Auth.create(config.getQiniuAccessKey(), config.getQiniuSecretKey()),new Configuration(Zone.autoZone()));
    }

    /**
     *删除文件
     * @param key
     */
    @Override
    public void delFile(String key){
        try {
            bucketManager.delete(config.getQiniuBucketName(), key);
        } catch (QiniuException ex) {
            //如果遇到异常，说明删除失败
            throw new UtilException("删除失败，"+ex.response.toString(), ex);
        }
    }

    @Override
    public String upload(byte[] data, String path) {
        try {
            Response res = uploadManager.put(data, path, token);
            if (!res.isOK()) {
                throw new RuntimeException("上传七牛出错：" + res.toString());
            }
        } catch (Exception e) {
            throw new UtilException("上传文件失败，请核对七牛配置信息", e);
        }

        return config.getQiniuDomain() + "/" + path;
    }

    @Override
    public String upload(InputStream inputStream, String path) {
        try {
            byte[] data = IOUtils.toByteArray(inputStream);
            return this.upload(data, path);
        } catch (IOException e) {
            throw new UtilException("上传文件失败", e);
        }
    }

    @Override
    public String uploadSuffix(byte[] data, String suffix) {
        return upload(data, getPath(config.getQiniuPrefix(), suffix));
    }

    @Override
    public String uploadSuffix(InputStream inputStream, String suffix) {
        return upload(inputStream, getPath(config.getQiniuPrefix(), suffix));
    }
}
