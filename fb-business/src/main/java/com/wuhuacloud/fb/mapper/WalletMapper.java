package com.wuhuacloud.fb.mapper;

import java.util.List;
import com.wuhuacloud.fb.domain.Wallet;

/**
 * 余额Mapper接口
 * 
 * @author wenfl
 * @date 2021-01-04
 */
public interface WalletMapper 
{
    /**
     * 查询余额
     * 
     * @param userId 余额ID
     * @return 余额
     */
    public Wallet selectWalletById(Long userId);

    /**
     * 查询余额列表
     * 
     * @param wallet 余额
     * @return 余额集合
     */
    public List<Wallet> selectWalletList(Wallet wallet);

    /**
     * 新增余额
     * 
     * @param wallet 余额
     * @return 结果
     */
    public int insertWallet(Wallet wallet);

    /**
     * 修改余额
     * 
     * @param wallet 余额
     * @return 结果
     */
    public int updateWallet(Wallet wallet);

    /**
     * 删除余额
     * 
     * @param userId 余额ID
     * @return 结果
     */
    public int deleteWalletById(Long userId);

    /**
     * 批量删除余额
     * 
     * @param userIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteWalletByIds(Long[] userIds);
}
