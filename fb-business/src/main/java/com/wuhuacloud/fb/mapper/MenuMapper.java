package com.wuhuacloud.fb.mapper;

import java.util.List;
import com.wuhuacloud.fb.domain.Menu;

/**
 * 食堂菜单Mapper接口
 * 
 * @author wenfl
 * @date 2020-12-23
 */
public interface MenuMapper 
{
    /**
     * 查询食堂菜单
     * 
     * @param menuId 食堂菜单ID
     * @return 食堂菜单
     */
    public Menu selectMenuById(Long menuId);

    /**
     * 查询食堂菜单列表
     * 
     * @param menu 食堂菜单
     * @return 食堂菜单集合
     */
    public List<Menu> selectMenuList(Menu menu);

    /**
     * 新增食堂菜单
     * 
     * @param menu 食堂菜单
     * @return 结果
     */
    public int insertMenu(Menu menu);

    /**
     * 修改食堂菜单
     * 
     * @param menu 食堂菜单
     * @return 结果
     */
    public int updateMenu(Menu menu);

    /**
     * 删除食堂菜单
     * 
     * @param menuId 食堂菜单ID
     * @return 结果
     */
    public int deleteMenuById(Long menuId);

    /**
     * 批量删除食堂菜单
     * 
     * @param menuIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteMenuByIds(Long[] menuIds);
}
