package com.wuhuacloud.fb.mapper;

import java.util.List;
import com.wuhuacloud.fb.domain.Suggest;

/**
 * 意见Mapper接口
 * 
 * @author wenfl
 * @date 2020-12-23
 */
public interface SuggestMapper 
{
    /**
     * 查询意见
     * 
     * @param suggestId 意见ID
     * @return 意见
     */
    public Suggest selectSuggestById(String suggestId);

    /**
     * 查询意见列表
     * 
     * @param suggest 意见
     * @return 意见集合
     */
    public List<Suggest> selectSuggestList(Suggest suggest);

    /**
     * 新增意见
     * 
     * @param suggest 意见
     * @return 结果
     */
    public int insertSuggest(Suggest suggest);

    /**
     * 修改意见
     * 
     * @param suggest 意见
     * @return 结果
     */
    public int updateSuggest(Suggest suggest);

    /**
     * 删除意见
     * 
     * @param suggestId 意见ID
     * @return 结果
     */
    public int deleteSuggestById(Long suggestId);

    /**
     * 批量删除意见
     * 
     * @param suggestIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSuggestByIds(Long[] suggestIds);
}
