package com.wuhuacloud.fb.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.wuhuacloud.common.annotation.Excel;
import com.wuhuacloud.common.core.domain.BaseEntity;

/**
 * 食堂菜单对象 fb_menu
 * 
 * @author wenfl
 * @date 2020-12-23
 */
public class Menu extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 菜单ID */
    private Long menuId;

    /** 菜单名称 */
    @Excel(name = "菜单名称")
    private String menuName;

    /** 菜单类型（荤菜、素菜、组合） */
    @Excel(name = "菜单类型", readConverterExp = "荤=菜、素菜、组合")
    private String menuType;

    /** 菜单图片 */
    @Excel(name = "菜单图片")
    private String menuImg;

    /** 显示顺序 */
    @Excel(name = "显示顺序")
    private Integer orderNum;

    /** 菜单价格 */
    @Excel(name = "菜单价格")
    private BigDecimal menuPrice;

    /** 菜单状态（0显示 1隐藏） */
    @Excel(name = "菜单状态", readConverterExp = "0=显示,1=隐藏")
    private String visible;

    /** 菜单状态（0正常 1停用） */
    @Excel(name = "菜单状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 食堂编号 */
    @Excel(name = "食堂编号")
    private Long canteenId;

    public void setMenuId(Long menuId) 
    {
        this.menuId = menuId;
    }

    public Long getMenuId() 
    {
        return menuId;
    }
    public void setMenuName(String menuName) 
    {
        this.menuName = menuName;
    }

    public String getMenuName() 
    {
        return menuName;
    }
    public void setMenuType(String menuType) 
    {
        this.menuType = menuType;
    }

    public String getMenuType() 
    {
        return menuType;
    }
    public void setMenuImg(String menuImg) 
    {
        this.menuImg = menuImg;
    }

    public String getMenuImg() 
    {
        return menuImg;
    }
    public void setOrderNum(Integer orderNum) 
    {
        this.orderNum = orderNum;
    }

    public Integer getOrderNum() 
    {
        return orderNum;
    }
    public void setMenuPrice(BigDecimal menuPrice) 
    {
        this.menuPrice = menuPrice;
    }

    public BigDecimal getMenuPrice() 
    {
        return menuPrice;
    }
    public void setVisible(String visible) 
    {
        this.visible = visible;
    }

    public String getVisible() 
    {
        return visible;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setCanteenId(Long canteenId) 
    {
        this.canteenId = canteenId;
    }

    public Long getCanteenId() 
    {
        return canteenId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("menuId", getMenuId())
            .append("menuName", getMenuName())
            .append("menuType", getMenuType())
            .append("menuImg", getMenuImg())
            .append("orderNum", getOrderNum())
            .append("menuPrice", getMenuPrice())
            .append("visible", getVisible())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("canteenId", getCanteenId())
            .toString();
    }
}
