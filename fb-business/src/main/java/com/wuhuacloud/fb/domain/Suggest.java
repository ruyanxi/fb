package com.wuhuacloud.fb.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.wuhuacloud.common.annotation.Excel;
import com.wuhuacloud.common.core.domain.BaseEntity;

/**
 * 意见对象 fb_suggest
 * 
 * @author wenfl
 * @date 2020-12-23
 */
public class Suggest extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String suggestId;

    /** 图片 */
    @Excel(name = "图片")
    private String img;

    /** 食堂编号 */
    @Excel(name = "食堂编号")
    private Long canteenId;

    /** 回复 */
    @Excel(name = "回复")
    private String replay;

    public void setSuggestId(String suggestId)
    {
        this.suggestId = suggestId;
    }

    public String getSuggestId()
    {
        return suggestId;
    }
    public void setImg(String img) 
    {
        this.img = img;
    }

    public String getImg() 
    {
        return img;
    }
    public void setCanteenId(Long canteenId) 
    {
        this.canteenId = canteenId;
    }

    public Long getCanteenId() 
    {
        return canteenId;
    }

    public void setReplay(String replay)
    {
        this.replay = replay;
    }

    public String getReplay()
    {
        return replay;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("suggestId", getSuggestId())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .append("img", getImg())
                .append("canteenId", getCanteenId())
                .append("replay", getReplay())
                .toString();
    }
}
