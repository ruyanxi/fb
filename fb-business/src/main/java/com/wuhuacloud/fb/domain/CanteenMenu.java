package com.wuhuacloud.fb.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.wuhuacloud.common.annotation.Excel;
import com.wuhuacloud.common.core.domain.BaseEntity;

/**
 * 食堂与菜单关联对象 fb_canteen_menu
 * 
 * @author wenfl
 * @date 2020-12-29
 */
public class CanteenMenu extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 菜单ID */
    @Excel(name = "菜单ID")
    private Long menuId;

    /** 食堂编号 */
    @Excel(name = "食堂编号")
    private Long canteenId;

    /** 餐类型（早中晚下午茶） */
    @Excel(name = "餐类型", readConverterExp = "早=中晚下午茶")
    private String mealType;

    /** 日期 */
    @Excel(name = "日期")
    private String mealDate;

    /** 菜单名称 */
    @Excel(name = "菜单名称")
    private String menuName;

    /** 菜单类型（荤菜、素菜、组合） */
    @Excel(name = "菜单类型", readConverterExp = "荤=菜、素菜、组合")
    private String menuType;

    /** 显示顺序 */
    @Excel(name = "显示顺序")
    private Integer orderNum;

    /** 菜单价格 */
    @Excel(name = "菜单价格")
    private BigDecimal menuPrice;

    /** 食堂价格 */
    @Excel(name = "食堂价格")
    private BigDecimal canteenPrice;

    /** 菜单图片 */
    @Excel(name = "菜单图片")
    private String menuImg;

    public void setMenuId(Long menuId) 
    {
        this.menuId = menuId;
    }

    public Long getMenuId() 
    {
        return menuId;
    }
    public void setCanteenId(Long canteenId) 
    {
        this.canteenId = canteenId;
    }

    public Long getCanteenId() 
    {
        return canteenId;
    }
    public void setMealType(String mealType) 
    {
        this.mealType = mealType;
    }

    public String getMealType() 
    {
        return mealType;
    }
    public void setMealDate(String mealDate) 
    {
        this.mealDate = mealDate;
    }

    public String getMealDate() 
    {
        return mealDate;
    }
    public void setMenuName(String menuName) 
    {
        this.menuName = menuName;
    }

    public String getMenuName() 
    {
        return menuName;
    }
    public void setMenuType(String menuType) 
    {
        this.menuType = menuType;
    }

    public String getMenuType() 
    {
        return menuType;
    }
    public void setOrderNum(Integer orderNum) 
    {
        this.orderNum = orderNum;
    }

    public Integer getOrderNum() 
    {
        return orderNum;
    }
    public void setMenuPrice(BigDecimal menuPrice) 
    {
        this.menuPrice = menuPrice;
    }

    public BigDecimal getMenuPrice() 
    {
        return menuPrice;
    }
    public void setCanteenPrice(BigDecimal canteenPrice) 
    {
        this.canteenPrice = canteenPrice;
    }

    public BigDecimal getCanteenPrice() 
    {
        return canteenPrice;
    }
    public void setMenuImg(String menuImg) 
    {
        this.menuImg = menuImg;
    }

    public String getMenuImg() 
    {
        return menuImg;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("menuId", getMenuId())
            .append("canteenId", getCanteenId())
            .append("mealType", getMealType())
            .append("mealDate", getMealDate())
            .append("menuName", getMenuName())
            .append("menuType", getMenuType())
            .append("orderNum", getOrderNum())
            .append("menuPrice", getMenuPrice())
            .append("canteenPrice", getCanteenPrice())
            .append("menuImg", getMenuImg())
            .toString();
    }
}
