package com.wuhuacloud.fb.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.wuhuacloud.common.annotation.Excel;
import com.wuhuacloud.common.core.domain.BaseEntity;

/**
 * 食堂对象 fb_canteen
 * 
 * @author wenfl
 * @date 2020-12-23
 */
public class Canteen extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 食堂编号 */
    private Long id;

    /** 食堂名称 */
    @Excel(name = "食堂名称")
    private String canteenName;

    /** 食堂电话 */
    @Excel(name = "食堂电话")
    private String canteenTel;

    /** 食堂地址 */
    @Excel(name = "食堂地址")
    private String canteenAddr;

    /** 食堂负责人 */
    @Excel(name = "食堂负责人")
    private String canteenPrincipal;

    /** 食堂经度 */
    @Excel(name = "食堂经度")
    private BigDecimal canteenLongitude;

    /** 食堂纬度 */
    @Excel(name = "食堂纬度")
    private BigDecimal canteenLatitude;

    /** 显示顺序 */
    @Excel(name = "显示顺序")
    private Integer canteenSort;

    /** 数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限） */
    @Excel(name = "数据范围", readConverterExp = "1=：全部数据权限,2=：自定数据权限,3=：本部门数据权限,4=：本部门及以下数据权限")
    private String dataScope;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String canteenStatus;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /**店铺logo**/
    private String logo;


    public void setLogo(String logo)
    {
        this.logo = logo;
    }

    public String getLogo()
    {
        return logo;
    }

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCanteenName(String canteenName) 
    {
        this.canteenName = canteenName;
    }

    public String getCanteenName() 
    {
        return canteenName;
    }
    public void setCanteenTel(String canteenTel) 
    {
        this.canteenTel = canteenTel;
    }

    public String getCanteenTel() 
    {
        return canteenTel;
    }
    public void setCanteenAddr(String canteenAddr) 
    {
        this.canteenAddr = canteenAddr;
    }

    public String getCanteenAddr() 
    {
        return canteenAddr;
    }
    public void setCanteenPrincipal(String canteenPrincipal) 
    {
        this.canteenPrincipal = canteenPrincipal;
    }

    public String getCanteenPrincipal() 
    {
        return canteenPrincipal;
    }
    public void setCanteenLongitude(BigDecimal canteenLongitude) 
    {
        this.canteenLongitude = canteenLongitude;
    }

    public BigDecimal getCanteenLongitude() 
    {
        return canteenLongitude;
    }
    public void setCanteenLatitude(BigDecimal canteenLatitude) 
    {
        this.canteenLatitude = canteenLatitude;
    }

    public BigDecimal getCanteenLatitude() 
    {
        return canteenLatitude;
    }
    public void setCanteenSort(Integer canteenSort) 
    {
        this.canteenSort = canteenSort;
    }

    public Integer getCanteenSort() 
    {
        return canteenSort;
    }
    public void setDataScope(String dataScope) 
    {
        this.dataScope = dataScope;
    }

    public String getDataScope() 
    {
        return dataScope;
    }
    public void setCanteenStatus(String canteenStatus) 
    {
        this.canteenStatus = canteenStatus;
    }

    public String getCanteenStatus() 
    {
        return canteenStatus;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("canteenName", getCanteenName())
            .append("canteenTel", getCanteenTel())
            .append("canteenAddr", getCanteenAddr())
            .append("canteenPrincipal", getCanteenPrincipal())
            .append("canteenLongitude", getCanteenLongitude())
            .append("canteenLatitude", getCanteenLatitude())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("canteenSort", getCanteenSort())
            .append("dataScope", getDataScope())
            .append("canteenStatus", getCanteenStatus())
            .append("delFlag", getDelFlag())
                .append("logo", getLogo())
            .toString();
    }
}
