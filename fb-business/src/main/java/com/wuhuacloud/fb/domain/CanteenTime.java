package com.wuhuacloud.fb.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.wuhuacloud.common.annotation.Excel;
import com.wuhuacloud.common.core.domain.BaseEntity;

/**
 * 食堂营业时间对象 fb_canteen_time
 * 
 * @author wenfl
 * @date 2021-01-06
 */
public class CanteenTime extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 食堂编号 */
    @Excel(name = "食堂编号")
    private Long canteenId;

    /** 餐类型（早中晚下午茶） */
    @Excel(name = "餐类型", readConverterExp = "早=中晚下午茶")
    private String mealType;

    /** 开始时间 */
    @Excel(name = "开始时间")
    private String startTime;

    /** 结束时间 */
    @Excel(name = "结束时间")
    private String stopTime;

    /** 显示顺序 */
    @Excel(name = "显示顺序")
    private Integer orderNum;

    public void setOrderNum(Integer orderNum)
    {
        this.orderNum = orderNum;
    }

    public Integer getOrderNum()
    {
        return orderNum;
    }

    public void setCanteenId(Long canteenId) 
    {
        this.canteenId = canteenId;
    }

    public Long getCanteenId() 
    {
        return canteenId;
    }
    public void setMealType(String mealType) 
    {
        this.mealType = mealType;
    }

    public String getMealType() 
    {
        return mealType;
    }
    public void setStartTime(String startTime) 
    {
        this.startTime = startTime;
    }

    public String getStartTime() 
    {
        return startTime;
    }
    public void setStopTime(String stopTime) 
    {
        this.stopTime = stopTime;
    }

    public String getStopTime() 
    {
        return stopTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("remark", getRemark())
            .append("canteenId", getCanteenId())
            .append("mealType", getMealType())
            .append("startTime", getStartTime())
            .append("stopTime", getStopTime())
            .toString();
    }
}
