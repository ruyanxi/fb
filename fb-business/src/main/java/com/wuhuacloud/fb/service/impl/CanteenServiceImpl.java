package com.wuhuacloud.fb.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wuhuacloud.common.utils.DateUtils;
import com.wuhuacloud.fb.domain.CanteenMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wuhuacloud.fb.mapper.CanteenMapper;
import com.wuhuacloud.fb.domain.Canteen;
import com.wuhuacloud.fb.service.ICanteenService;

/**
 * 食堂Service业务层处理
 * 
 * @author wenfl
 * @date 2020-12-23
 */
@Service
public class CanteenServiceImpl implements ICanteenService 
{
    @Autowired
    private CanteenMapper canteenMapper;

    /**
     * 查询食堂
     * 
     * @param id 食堂ID
     * @return 食堂
     */
    @Override
    public Canteen selectCanteenById(Long id)
    {
        return canteenMapper.selectCanteenById(id);
    }

    /**
     * 查询食堂列表
     * 
     * @param canteen 食堂
     * @return 食堂
     */
    @Override
    public List<Canteen> selectCanteenList(Canteen canteen)
    {
        return canteenMapper.selectCanteenList(canteen);
    }

    /**
     * 新增食堂
     * 
     * @param canteen 食堂
     * @return 结果
     */
    @Override
    public int insertCanteen(Canteen canteen)
    {
        canteen.setCreateTime(DateUtils.getNowDate());
        return canteenMapper.insertCanteen(canteen);
    }

    /**
     * 修改食堂
     * 
     * @param canteen 食堂
     * @return 结果
     */
    @Override
    public int updateCanteen(Canteen canteen)
    {
        canteen.setUpdateTime(DateUtils.getNowDate());
        return canteenMapper.updateCanteen(canteen);
    }

    /**
     * 批量删除食堂
     * 
     * @param ids 需要删除的食堂ID
     * @return 结果
     */
    @Override
    public int deleteCanteenByIds(Long[] ids)
    {
        return canteenMapper.deleteCanteenByIds(ids);
    }

    /**
     * 删除食堂信息
     * 
     * @param id 食堂ID
     * @return 结果
     */
    @Override
    public int deleteCanteenById(Long id)
    {
        return canteenMapper.deleteCanteenById(id);
    }

    @Override
    public JSONObject getDayCanteenMenu(String mealDate, String mealType,Long canteenId) {

        JSONObject jsonObject =new JSONObject();

        CanteenMenu canteenMenu = new CanteenMenu();
        canteenMenu.setMealDate(mealDate);
        canteenMenu.setCanteenId(canteenId);
        //早中晚...
        canteenMenu.setMealType(mealType);
        JSONObject mealTypeJson = new JSONObject();
        List<CanteenMenu> canteenMenuList = canteenMapper.getDayCanteenMenu(canteenMenu);
        if(canteenMenuList.size()==0){
            mealTypeJson.put(mealType,new ArrayList());
        }else{
            mealTypeJson.put(mealType,canteenMenuList);
        }

        jsonObject.put(mealDate,mealTypeJson);

        return jsonObject;
    }

    @Override
    public void editDayCanteenMenu(Map dataMap) {

        String date=dataMap.get("mealDateFont").toString();
        String mealType=dataMap.get("mealType").toString();
        //先删除已有数据
        canteenMapper.delCanteenMenut(dataMap);
        //解析数据
        JSONObject dataJson =JSONObject.parseObject(JSON.toJSONString(dataMap.get("data")));
        JSONArray nData = dataJson.getJSONObject(date).getJSONArray(mealType);
        for (Iterator<Object> iterator = nData.iterator(); iterator.hasNext(); ) {
            JSONObject next = (JSONObject) iterator.next();


            CanteenMenu canteenMenu = new CanteenMenu();
            canteenMenu.setCanteenId(Long.parseLong(dataMap.get("canteenId").toString()));
            canteenMenu.setMealDate(date);
            canteenMenu.setMealType(mealType);

            canteenMenu.setMenuId(next.getLong("menuId"));
            canteenMenu.setMenuType(next.getString("menuType"));
            canteenMenu.setMenuName(next.getString("menuName"));
            canteenMenu.setOrderNum(next.getInteger("orderNum"));
            canteenMenu.setMenuPrice(next.getBigDecimal("menuPrice"));
            canteenMenu.setMenuImg(next.getString("menuImg"));
            canteenMenu.setCanteenPrice(next.getBigDecimal("canteenPrice"));
            canteenMapper.editDayCanteenMenu(canteenMenu);
        }
    }

    @Override
    public void delCanteenMenut(Map dataMap) {
        canteenMapper.delCanteenMenut(dataMap);
    }

    @Override
    public List<Canteen> selectCanteenDept(Long deptId) {
        return canteenMapper.selectCanteenDept(deptId);
    }

    @Override
    public void delCanteenDeop(Long deptId) {
        canteenMapper.delCanteenDeop(deptId);
    }

    @Override
    public void saveCanteenDeop(Long deptId, String canteenId) {
        canteenMapper.saveCanteenDeop(deptId,canteenId);
    }

    /**
     * 查询食堂与菜单关联列表
     *
     * @param canteenMenu 食堂与菜单关联
     * @return 食堂与菜单关联
     */
    @Override
    public List<CanteenMenu> selectCanteenMenuList(CanteenMenu canteenMenu)
    {
        return canteenMapper.selectCanteenMenuList(canteenMenu);
    }
}
