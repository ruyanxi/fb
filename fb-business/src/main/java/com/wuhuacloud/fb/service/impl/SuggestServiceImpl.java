package com.wuhuacloud.fb.service.impl;

import java.util.List;
import com.wuhuacloud.common.utils.DateUtils;
import com.wuhuacloud.common.utils.uuid.IdUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wuhuacloud.fb.mapper.SuggestMapper;
import com.wuhuacloud.fb.domain.Suggest;
import com.wuhuacloud.fb.service.ISuggestService;

/**
 * 意见Service业务层处理
 * 
 * @author wenfl
 * @date 2020-12-23
 */
@Service
public class SuggestServiceImpl implements ISuggestService 
{
    @Autowired
    private SuggestMapper suggestMapper;

    /**
     * 查询意见
     * 
     * @param suggestId 意见ID
     * @return 意见
     */
    @Override
    public Suggest selectSuggestById(String suggestId)
    {
        return suggestMapper.selectSuggestById(suggestId);
    }

    /**
     * 查询意见列表
     * 
     * @param suggest 意见
     * @return 意见
     */
    @Override
    public List<Suggest> selectSuggestList(Suggest suggest)
    {
        return suggestMapper.selectSuggestList(suggest);
    }

    /**
     * 新增意见
     * 
     * @param suggest 意见
     * @return 结果
     */
    @Override
    public int insertSuggest(Suggest suggest)
    {
        suggest.setSuggestId(IdUtils.simpleUUID());
        suggest.setCreateTime(DateUtils.getNowDate());
        return suggestMapper.insertSuggest(suggest);
    }

    /**
     * 修改意见
     * 
     * @param suggest 意见
     * @return 结果
     */
    @Override
    public int updateSuggest(Suggest suggest)
    {
        suggest.setUpdateTime(DateUtils.getNowDate());
        return suggestMapper.updateSuggest(suggest);
    }

    /**
     * 批量删除意见
     * 
     * @param suggestIds 需要删除的意见ID
     * @return 结果
     */
    @Override
    public int deleteSuggestByIds(Long[] suggestIds)
    {
        return suggestMapper.deleteSuggestByIds(suggestIds);
    }

    /**
     * 删除意见信息
     * 
     * @param suggestId 意见ID
     * @return 结果
     */
    @Override
    public int deleteSuggestById(Long suggestId)
    {
        return suggestMapper.deleteSuggestById(suggestId);
    }
}
