package com.wuhuacloud.fb.service;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.wuhuacloud.fb.domain.Canteen;
import com.wuhuacloud.fb.domain.CanteenMenu;

/**
 * 食堂Service接口
 * 
 * @author wenfl
 * @date 2020-12-23
 */
public interface ICanteenService 
{
    /**
     * 查询食堂
     * 
     * @param id 食堂ID
     * @return 食堂
     */
    public Canteen selectCanteenById(Long id);

    /**
     * 查询食堂列表
     * 
     * @param canteen 食堂
     * @return 食堂集合
     */
    public List<Canteen> selectCanteenList(Canteen canteen);

    /**
     * 新增食堂
     * 
     * @param canteen 食堂
     * @return 结果
     */
    public int insertCanteen(Canteen canteen);

    /**
     * 修改食堂
     * 
     * @param canteen 食堂
     * @return 结果
     */
    public int updateCanteen(Canteen canteen);

    /**
     * 批量删除食堂
     * 
     * @param ids 需要删除的食堂ID
     * @return 结果
     */
    public int deleteCanteenByIds(Long[] ids);

    /**
     * 删除食堂信息
     * 
     * @param id 食堂ID
     * @return 结果
     */
    public int deleteCanteenById(Long id);

    JSONObject getDayCanteenMenu(String mealDate, String mealType,Long canteenId);

    void editDayCanteenMenu(Map dataMap);

    void delCanteenMenut(Map dataMap);

    List<Canteen> selectCanteenDept(Long deptId);

    void delCanteenDeop(Long deptId);

    void saveCanteenDeop(Long deptId, String canteenId);

    /**
     * 查询食堂与菜单关联列表
     *
     * @param canteenMenu 食堂与菜单关联
     * @return 食堂与菜单关联集合
     */
    public List<CanteenMenu> selectCanteenMenuList(CanteenMenu canteenMenu);
}
