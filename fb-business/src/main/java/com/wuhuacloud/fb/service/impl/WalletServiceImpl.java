package com.wuhuacloud.fb.service.impl;

import java.util.List;
import com.wuhuacloud.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wuhuacloud.fb.mapper.WalletMapper;
import com.wuhuacloud.fb.domain.Wallet;
import com.wuhuacloud.fb.service.IWalletService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 余额Service业务层处理
 * 
 * @author wenfl
 * @date 2021-01-04
 */
@Service
public class WalletServiceImpl implements IWalletService 
{
    @Autowired
    private WalletMapper walletMapper;

    /**
     * 查询余额
     * 
     * @param userId 余额ID
     * @return 余额
     */
    @Override
    public Wallet selectWalletById(Long userId)
    {
        return walletMapper.selectWalletById(userId);
    }

    /**
     * 查询余额列表
     * 
     * @param wallet 余额
     * @return 余额
     */
    @Override
    public List<Wallet> selectWalletList(Wallet wallet)
    {
        return walletMapper.selectWalletList(wallet);
    }

    /**
     * 新增余额
     * 
     * @param wallet 余额
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertWallet(Wallet wallet)
    {
        wallet.setCreateTime(DateUtils.getNowDate());
        return walletMapper.insertWallet(wallet);
    }

    /**
     * 修改余额
     * 
     * @param wallet 余额
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateWallet(Wallet wallet)
    {
        wallet.setUpdateTime(DateUtils.getNowDate());
        return walletMapper.updateWallet(wallet);
    }

    /**
     * 批量删除余额
     * 
     * @param userIds 需要删除的余额ID
     * @return 结果
     */
    @Override
    public int deleteWalletByIds(Long[] userIds)
    {
        return walletMapper.deleteWalletByIds(userIds);
    }

    /**
     * 删除余额信息
     * 
     * @param userId 余额ID
     * @return 结果
     */
    @Override
    public int deleteWalletById(Long userId)
    {
        return walletMapper.deleteWalletById(userId);
    }
}
