package com.wuhuacloud.fb.service.impl;

import java.util.List;
import com.wuhuacloud.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wuhuacloud.fb.mapper.CanteenTimeMapper;
import com.wuhuacloud.fb.domain.CanteenTime;
import com.wuhuacloud.fb.service.ICanteenTimeService;

/**
 * 食堂营业时间Service业务层处理
 * 
 * @author wenfl
 * @date 2021-01-06
 */
@Service
public class CanteenTimeServiceImpl implements ICanteenTimeService 
{
    @Autowired
    private CanteenTimeMapper canteenTimeMapper;

    /**
     * 查询食堂营业时间
     * 
     * @param canteenId 食堂营业时间ID
     * @return 食堂营业时间
     */
    @Override
    public CanteenTime selectCanteenTimeById(String canteenId)
    {
        return canteenTimeMapper.selectCanteenTimeById(canteenId);
    }

    /**
     * 查询食堂营业时间列表
     * 
     * @param canteenTime 食堂营业时间
     * @return 食堂营业时间
     */
    @Override
    public List<CanteenTime> selectCanteenTimeList(CanteenTime canteenTime)
    {
        return canteenTimeMapper.selectCanteenTimeList(canteenTime);
    }

    /**
     * 新增食堂营业时间
     * 
     * @param canteenTime 食堂营业时间
     * @return 结果
     */
    @Override
    public int insertCanteenTime(CanteenTime canteenTime)
    {
        canteenTime.setCreateTime(DateUtils.getNowDate());
        return canteenTimeMapper.insertCanteenTime(canteenTime);
    }

    /**
     * 修改食堂营业时间
     * 
     * @param canteenTime 食堂营业时间
     * @return 结果
     */
    @Override
    public int updateCanteenTime(CanteenTime canteenTime)
    {
        return canteenTimeMapper.updateCanteenTime(canteenTime);
    }

    /**
     * 批量删除食堂营业时间
     * 
     * @param canteenIds 需要删除的食堂营业时间ID
     * @return 结果
     */
    @Override
    public int deleteCanteenTimeByIds(String[] canteenIds)
    {
        return canteenTimeMapper.deleteCanteenTimeByIds(canteenIds);
    }

    /**
     * 删除食堂营业时间信息
     * 
     * @param canteenId 食堂营业时间ID
     * @return 结果
     */
    @Override
    public int deleteCanteenTimeById(String canteenId)
    {
        return canteenTimeMapper.deleteCanteenTimeById(canteenId);
    }
}
