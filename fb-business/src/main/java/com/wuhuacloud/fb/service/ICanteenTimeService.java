package com.wuhuacloud.fb.service;

import java.util.List;
import com.wuhuacloud.fb.domain.CanteenTime;

/**
 * 食堂营业时间Service接口
 * 
 * @author wenfl
 * @date 2021-01-06
 */
public interface ICanteenTimeService 
{
    /**
     * 查询食堂营业时间
     * 
     * @param canteenId 食堂营业时间ID
     * @return 食堂营业时间
     */
    public CanteenTime selectCanteenTimeById(String canteenId);

    /**
     * 查询食堂营业时间列表
     * 
     * @param canteenTime 食堂营业时间
     * @return 食堂营业时间集合
     */
    public List<CanteenTime> selectCanteenTimeList(CanteenTime canteenTime);

    /**
     * 新增食堂营业时间
     * 
     * @param canteenTime 食堂营业时间
     * @return 结果
     */
    public int insertCanteenTime(CanteenTime canteenTime);

    /**
     * 修改食堂营业时间
     * 
     * @param canteenTime 食堂营业时间
     * @return 结果
     */
    public int updateCanteenTime(CanteenTime canteenTime);

    /**
     * 批量删除食堂营业时间
     * 
     * @param canteenIds 需要删除的食堂营业时间ID
     * @return 结果
     */
    public int deleteCanteenTimeByIds(String[] canteenIds);

    /**
     * 删除食堂营业时间信息
     * 
     * @param canteenId 食堂营业时间ID
     * @return 结果
     */
    public int deleteCanteenTimeById(String canteenId);
}
