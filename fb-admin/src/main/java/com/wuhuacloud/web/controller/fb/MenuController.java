package com.wuhuacloud.web.controller.fb;

import com.wuhuacloud.common.annotation.Log;
import com.wuhuacloud.common.core.controller.BaseController;
import com.wuhuacloud.common.core.domain.AjaxResult;
import com.wuhuacloud.common.core.page.TableDataInfo;
import com.wuhuacloud.common.enums.BusinessType;
import com.wuhuacloud.common.utils.poi.ExcelUtil;
import com.wuhuacloud.fb.domain.Menu;
import com.wuhuacloud.fb.service.IMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 食堂菜单Controller
 * 
 * @author wenfl
 * @date 2020-12-23
 */
@RestController
@RequestMapping("/fb/menu")
public class MenuController extends BaseController
{
    @Autowired
    private IMenuService menuService;

    /**
     * 查询食堂菜单列表
     */
    @PreAuthorize("@ss.hasPermi('fb:menu:list')")
    @GetMapping("/list")
    public TableDataInfo list(Menu menu)
    {
        startPage();
        List<Menu> list = menuService.selectMenuList(menu);
        return getDataTable(list);
    }

    /**
     * 导出食堂菜单列表
     */
    @PreAuthorize("@ss.hasPermi('fb:menu:export')")
    @Log(title = "食堂菜单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Menu menu)
    {
        List<Menu> list = menuService.selectMenuList(menu);
        ExcelUtil<Menu> util = new ExcelUtil<Menu>(Menu.class);
        return util.exportExcel(list, "menu");
    }

    /**
     * 获取食堂菜单详细信息
     */
    @PreAuthorize("@ss.hasPermi('fb:menu:query')")
    @GetMapping(value = "/{menuId}")
    public AjaxResult getInfo(@PathVariable("menuId") Long menuId)
    {
        return AjaxResult.success(menuService.selectMenuById(menuId));
    }

    /**
     * 新增食堂菜单
     */
    @PreAuthorize("@ss.hasPermi('fb:menu:add')")
    @Log(title = "食堂菜单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Menu menu)
    {
        return toAjax(menuService.insertMenu(menu));
    }

    /**
     * 修改食堂菜单
     */
    @PreAuthorize("@ss.hasPermi('fb:menu:edit')")
    @Log(title = "食堂菜单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Menu menu)
    {
        return toAjax(menuService.updateMenu(menu));
    }

    /**
     * 删除食堂菜单
     */
    @PreAuthorize("@ss.hasPermi('fb:menu:remove')")
    @Log(title = "食堂菜单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{menuIds}")
    public AjaxResult remove(@PathVariable Long[] menuIds)
    {
        return toAjax(menuService.deleteMenuByIds(menuIds));
    }
}
