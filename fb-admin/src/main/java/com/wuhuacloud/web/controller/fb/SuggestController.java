package com.wuhuacloud.web.controller.fb;

import com.wuhuacloud.common.annotation.Log;
import com.wuhuacloud.common.core.controller.BaseController;
import com.wuhuacloud.common.core.domain.AjaxResult;
import com.wuhuacloud.common.core.page.TableDataInfo;
import com.wuhuacloud.common.enums.BusinessType;
import com.wuhuacloud.common.utils.poi.ExcelUtil;
import com.wuhuacloud.fb.domain.Suggest;
import com.wuhuacloud.fb.service.ISuggestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 意见Controller
 * 
 * @author wenfl
 * @date 2020-12-23
 */
@RestController
@RequestMapping("/fb/suggest")
public class SuggestController extends BaseController
{
    @Autowired
    private ISuggestService suggestService;

    /**
     * 查询意见列表
     */
    @PreAuthorize("@ss.hasPermi('fb:suggest:list')")
    @GetMapping("/list")
    public TableDataInfo list(Suggest suggest)
    {
        startPage();
        List<Suggest> list = suggestService.selectSuggestList(suggest);
        return getDataTable(list);
    }

    /**
     * 导出意见列表
     */
    @PreAuthorize("@ss.hasPermi('fb:suggest:export')")
    @Log(title = "意见", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Suggest suggest)
    {
        List<Suggest> list = suggestService.selectSuggestList(suggest);
        ExcelUtil<Suggest> util = new ExcelUtil<Suggest>(Suggest.class);
        return util.exportExcel(list, "suggest");
    }

    /**
     * 获取意见详细信息
     */
    @PreAuthorize("@ss.hasPermi('fb:suggest:query')")
    @GetMapping(value = "/{suggestId}")
    public AjaxResult getInfo(@PathVariable("suggestId") String suggestId)
    {
        return AjaxResult.success(suggestService.selectSuggestById(suggestId));
    }

    /**
     * 新增意见
     */
    @PreAuthorize("@ss.hasPermi('fb:suggest:add')")
    @Log(title = "意见", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Suggest suggest)
    {
        return toAjax(suggestService.insertSuggest(suggest));
    }

    /**
     * 修改意见
     */
    @PreAuthorize("@ss.hasPermi('fb:suggest:edit')")
    @Log(title = "意见", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Suggest suggest)
    {
        return toAjax(suggestService.updateSuggest(suggest));
    }

    /**
     * 删除意见
     */
    @PreAuthorize("@ss.hasPermi('fb:suggest:remove')")
    @Log(title = "意见", businessType = BusinessType.DELETE)
	@DeleteMapping("/{suggestIds}")
    public AjaxResult remove(@PathVariable Long[] suggestIds)
    {
        return toAjax(suggestService.deleteSuggestByIds(suggestIds));
    }
}
