package com.wuhuacloud.web.controller.fb;

import com.wuhuacloud.common.annotation.Log;
import com.wuhuacloud.common.core.controller.BaseController;
import com.wuhuacloud.common.core.domain.AjaxResult;
import com.wuhuacloud.common.core.page.TableDataInfo;
import com.wuhuacloud.common.enums.BusinessType;
import com.wuhuacloud.common.utils.poi.ExcelUtil;
import com.wuhuacloud.fb.domain.TradeInfo;
import com.wuhuacloud.fb.service.ITradeInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 交易记录Controller
 * 
 * @author wenfl
 * @date 2021-01-04
 */
@RestController
@RequestMapping("/fb/info")
public class TradeInfoController extends BaseController
{
    @Autowired
    private ITradeInfoService tradeInfoService;

    /**
     * 查询交易记录列表
     */
    @PreAuthorize("@ss.hasPermi('fb:info:list')")
    @GetMapping("/list")
    public TableDataInfo list(TradeInfo tradeInfo)
    {
        startPage();
        List<TradeInfo> list = tradeInfoService.selectTradeInfoList(tradeInfo);
        return getDataTable(list);
    }

    /**
     * 导出交易记录列表
     */
    @PreAuthorize("@ss.hasPermi('fb:info:export')")
    @Log(title = "交易记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TradeInfo tradeInfo)
    {
        List<TradeInfo> list = tradeInfoService.selectTradeInfoList(tradeInfo);
        ExcelUtil<TradeInfo> util = new ExcelUtil<TradeInfo>(TradeInfo.class);
        return util.exportExcel(list, "info");
    }

    /**
     * 获取交易记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('fb:info:query')")
    @GetMapping(value = "/{tradeId}")
    public AjaxResult getInfo(@PathVariable("tradeId") Long tradeId)
    {
        return AjaxResult.success(tradeInfoService.selectTradeInfoById(tradeId));
    }
}
