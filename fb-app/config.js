// import store from './common/store/store.js'

export default {
	//导航栏颜色
	// background:{backgroundImage: 'linear-gradient(45deg, rgb(28, 187, 180), rgb(141, 198, 63))'}
	//微信小程序视图里，必须用函数，才能生效，js里不受限制
	
	background: function() {
		return {
			background: '#FF6364'
		}
	}, //标题背景颜色
	titleColor: function() {
		return "#FFFFFF"
	}, //白色文字颜色
	activeColor: function() {
		return "#FF6364"
	}, //红色文字颜色
	//后台接口地址
	server: 'https://127.0.0.1/api',
	//后台文件上传地址
	serverUpload: 'https://127.0.0.1/common/upload',
	jump: (url, data = {}) => {
		if (typeof data !== 'object') {
			throw '第二个参数需为对象'
		}
		if (data) {
			let pre = '?';
			let keys = Object.keys(data);
			keys.forEach(key => {
				pre += key + '=' + data[key] + '&'
			})
			pre = pre.slice(0, -1);
			url = url + pre
		}
		uni.navigateTo({
			url: url
		})
	},
	showtoast: title => {
		uni.showToast({
			icon: 'none',
			title: title,
			duration: 2000
		})
	},
	getLength: function getLength(str) {
		return str.replace(/[^\x00-\xff]/g, '..').length;
	},
	showLoading: msg => {
		uni.showLoading({
			title: msg,
			mask: true
		});
	},
	addSouSuoKey: ss => {
		let sousuokey = uni.getStorageSync('sousuokey') ? uni.getStorageSync('sousuokey') : [];
		let aa = false;
		sousuokey.forEach(val => {
			if (val == ss) {
				aa = true;
			}
		})
		if (aa) {
			return
		}
		if (sousuokey.length >= 10) {
			sousuokey.shift()
		}
		sousuokey.push(ss);
		uni.setStorageSync('sousuokey', sousuokey)
	},
	//百度坐标转高德（传入经度、纬度）
	gps_bd_gd:function(bd_lng, bd_lat) {
	    var X_PI = Math.PI * 3000.0 / 180.0;
	    var x = bd_lng - 0.0065;
	    var y = bd_lat - 0.006;
	    var z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * X_PI);
	    var theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * X_PI);
	    var gd_lng = z * Math.cos(theta);
	    var gd_lat = z * Math.sin(theta);
	    return { lon: gd_lng, lat: gd_lat }
	},
	//高德坐标转百度（传入经度、纬度）
	gps_gd_bd:function(gd_lng, gd_lat) {
	    var X_PI = Math.PI * 3000.0 / 180.0;
	    var x = gd_lng, y = gd_lat;
	    var z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * X_PI);
	    var theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * X_PI);
	    var bd_lng = z * Math.cos(theta) + 0.0065;
	    var bd_lat = z * Math.sin(theta) + 0.006;
	    return {
	        lat: bd_lat,
	        lon: bd_lng
	    };
	},
	//px转rpx
	pxtorpx:function(px){
		let rpx= px * (750/uni.getSystemInfoSync().windowWidth);
		return rpx
	},
	//rpx转px
	rpxtopx:function(rpx){
		
		let px = rpx * (uni.getSystemInfoSync().windowWidth/750);
		
		
		return px
	},
}
