import request from '@/utils/request'

// 查询文件信息列表
export function listFileinfo(query) {
  return request({
    url: '/system/fileinfo/list',
    method: 'get',
    params: query
  })
}

// 查询文件信息详细
export function getFileinfo(fileId) {
  return request({
    url: '/system/fileinfo/' + fileId,
    method: 'get'
  })
}

// 新增文件信息
export function addFileinfo(data) {
  return request({
    url: '/system/fileinfo',
    method: 'post',
    data: data
  })
}

// 修改文件信息
export function updateFileinfo(data) {
  return request({
    url: '/system/fileinfo',
    method: 'put',
    data: data
  })
}

// 删除文件信息
export function delFileinfo(fileId) {
  return request({
    url: '/system/fileinfo/' + fileId,
    method: 'delete'
  })
}

// 导出文件信息
export function exportFileinfo(query) {
  return request({
    url: '/system/fileinfo/export',
    method: 'get',
    params: query
  })
}

//获取配置
export function getConfig(){
  return request({
    url: '/system/fileinfo/config',
    method: 'get'
  })
}

//保存配置
export function saveConfig(data) {
  return request({
    url: '/system/fileinfo/saveConfig',
    method: 'post',
    data: data
  })
}