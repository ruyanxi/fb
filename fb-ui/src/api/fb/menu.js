import request from '@/utils/request'

// 查询食堂菜单列表
export function listMenu(query) {
  return request({
    url: '/fb/menu/list',
    method: 'get',
    params: query
  })
}

// 查询食堂菜单详细
export function getMenu(menuId) {
  return request({
    url: '/fb/menu/' + menuId,
    method: 'get'
  })
}

// 新增食堂菜单
export function addMenu(data) {
  return request({
    url: '/fb/menu',
    method: 'post',
    data: data
  })
}

// 修改食堂菜单
export function updateMenu(data) {
  return request({
    url: '/fb/menu',
    method: 'put',
    data: data
  })
}

// 删除食堂菜单
export function delMenu(menuId) {
  return request({
    url: '/fb/menu/' + menuId,
    method: 'delete'
  })
}

// 导出食堂菜单
export function exportMenu(query) {
  return request({
    url: '/fb/menu/export',
    method: 'get',
    params: query
  })
}