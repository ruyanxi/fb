import request from '@/utils/request'

// 查询意见列表
export function listSuggest(query) {
  return request({
    url: '/fb/suggest/list',
    method: 'get',
    params: query
  })
}

// 查询意见详细
export function getSuggest(suggestId) {
  return request({
    url: '/fb/suggest/' + suggestId,
    method: 'get'
  })
}

// 新增意见
export function addSuggest(data) {
  return request({
    url: '/fb/suggest',
    method: 'post',
    data: data
  })
}

// 修改意见
export function updateSuggest(data) {
  return request({
    url: '/fb/suggest',
    method: 'put',
    data: data
  })
}

// 删除意见
export function delSuggest(suggestId) {
  return request({
    url: '/fb/suggest/' + suggestId,
    method: 'delete'
  })
}

// 导出意见
export function exportSuggest(query) {
  return request({
    url: '/fb/suggest/export',
    method: 'get',
    params: query
  })
}