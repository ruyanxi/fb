import request from '@/utils/request'

// 查询食堂列表
export function listCanteen(query) {
  return request({
    url: '/fb/canteen/list',
    method: 'get',
    params: query
  })
}

// 查询食堂列表
export function listCanteenAll() {
  return request({
    url: '/fb/canteen/list/all',
    method: 'get',
    params: {}
  })
}

// 查询食堂详细
export function getCanteen(id) {
  return request({
    url: '/fb/canteen/' + id,
    method: 'get'
  })
}

// 新增食堂
export function addCanteen(data) {
  return request({
    url: '/fb/canteen',
    method: 'post',
    data: data
  })
}

// 修改食堂
export function updateCanteen(data) {
  return request({
    url: '/fb/canteen',
    method: 'put',
    data: data
  })
}

// 删除食堂
export function delCanteen(id) {
  return request({
    url: '/fb/canteen/' + id,
    method: 'delete'
  })
}

// 导出食堂
export function exportCanteen(query) {
  return request({
    url: '/fb/canteen/export',
    method: 'get',
    params: query
  })  
}

//查询食堂绑定的菜单
export function getDayCanteenMenu(query) {
  return request({
    url: '/fb/canteen/day/menu',
    method: 'get',
    params: query
  })
}

//新增修改食堂绑定菜单
export function editDayCanteenMenu(data) {
  return request({
    url: '/fb/canteen/day/menu',
    method: 'post',
    data: data
  })
}

// 删除食堂绑定的菜单
export function delCanteenMenut(data) {
  return request({
    url: '/fb/canteen/day/menu/del',
    method: 'post',
    data: data
  })
}

//查询食堂营业时间
export function setCanteenTime(canteenId){
  return request({
    url: '/fb/canteen_time/' + canteenId,
    method: 'get'
  })
}

//保存食堂营业时间
export function saveCanteenTime(data){
  return request({
    url: '/fb/canteen_time',
    method: 'post',
    data: data
  })
}

//查询该部门下已有的食堂信息
export function selectCanteenDept(deptId){
  return request({
    url: '/fb/canteen/selectCanteenDept/' + deptId,
    method: 'get'
  })
}

//保存食堂与部门关联值
export function saveCanteenDept(data){
  return request({
    url: '/fb/canteen/saveCanteenDept',
    method: 'post',
    data: data
  })
}